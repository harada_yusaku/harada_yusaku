package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {  //ユーザー新規登録のDao


    	PreparedStatement ps = null;
    	try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
//                    System.out.println(ps +"新規登録");

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranch_id());
            ps.setString(5, user.getPosition_id());
            ps.setString(6, user.getIs_stopped());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    	return ;



    }







	public User getUser(Connection connection, String account,String password) {  //ログインのDao

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
        	sql.append("SELECT ");
        	sql.append(" users.id as id,");
			sql.append(" users.account as account,");
			sql.append(" users.name as name,");
			sql.append(" branches.name as branch_name,");
			sql.append(" users.branch_id as branch_id,");
			sql.append(" positions.name as position_name,");
			sql.append(" users.position_id as position_id,");
			sql.append(" users.is_stopped");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches");
			sql.append(" ON users.branch_id = branches.id");
			sql.append(" INNER JOIN positions");
			sql.append(" ON users.position_id = positions.id");
			sql.append(" AND account = ? AND password = ? AND is_stopped = 0");
            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, account);
            ps.setString(2, password);


            ResultSet rs = ps.executeQuery();
            System.out.println(ps+"loginDao");

            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");
//                String password = rs.getString("password");
                String name = rs.getString("name");
                String branchName = rs.getString("branch_name");
                String branch_id = rs.getString("branch_id");
                String positionName = rs.getString("position_name");
                String position_id = rs.getString("position_id");
                String is_stopped = rs.getString("is_stopped");
//                Timestamp createdDate = rs.getTimestamp("created_date");
//                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
//                user.setPassword(password);
                user.setName(name);
                user.setBranchName(branchName);
                user.setBranch_id(branch_id);
                user.setPositionName(positionName);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
//                user.setCreatedDate(createdDate);
//                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }



    public User getUser(Connection connection , int id) {  //ユーザーごとの編集画面に表示するDao
    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE id=?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
            List<User> ret = editUserList(rs);

            if (ret.isEmpty() == true) {
            	return null;
            }else if (2<=ret.size()) {
            	throw new IllegalStateException("2 <= userList.size()");
            }else {
            	return ret.get(0);
            }
    	} catch (SQLException e){
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
    private List<User> editUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String account = rs.getString("account");
//                String password = rs.getString("password");
                String name = rs.getString("name");
//                String branchName = rs.getString("branch_name");
                String branch_id = rs.getString("branch_id");
//                String positionName = rs.getString("position_name");
                String position_id = rs.getString("position_id");
                String is_stopped = rs.getString("is_stopped");
//                Timestamp createdDate = rs.getTimestamp("created_date");
//                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
//                user.setPassword(password);
                user.setName(name);
//                user.setBranchName(branchName);
                user.setBranch_id(branch_id);
//                user.setPositionName(positionName);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
//                user.setCreatedDate(createdDate);
//                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    public void userUpdate(Connection connection, User edit) {  //ユーザー編集でUPDATEするDao
    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE users SET ");
    		sql.append("name = ?");
	    	sql.append(", account = ?");
	    	if (!StringUtils.isBlank(edit.getPassword())) {
	    		sql.append(", password = ?");
	    	}
    		sql.append(", branch_id = ?");
    		sql.append(", position_id = ?");
    		sql.append(" WHERE id = ?");

    		ps = connection.prepareStatement(sql.toString());
//    		System.out.println(ps +"ユーザー編集");

    		ps.setString(1, edit.getName());
    		ps.setString(2, edit.getAccount());
    		if(!StringUtils.isBlank(edit.getPassword())) {
	    		ps.setString(3, edit.getPassword());
	    		ps.setString(4, edit.getBranch_id());
	    		ps.setString(5, edit.getPosition_id());
	    		ps.setInt(6, edit.getId());
    		}else {
    			ps.setString(3, edit.getBranch_id());
	    		ps.setString(4, edit.getPosition_id());
	    		ps.setInt(5, edit.getId());
    		}
    		ps.executeUpdate();
    	} catch (SQLException e){
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }



    public void change(Connection connection, User isStopped) {
    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE users SET is_stopped = ? WHERE id = ?");

    		ps=connection.prepareStatement(sql.toString());


    		ps.setString(1, isStopped.getIs_stopped());
    		ps.setInt(2, isStopped.getId());
//    		System.out.println(ps +"停止復活");
    		ps.executeUpdate();
    	}
    	 catch (SQLException e) {
 			throw new SQLRuntimeException(e);
 		} finally {
 			close(ps);
 		}
    }

    public User getCheckUser(Connection connection, User accountCheck) {  //新規登録重複チェック
    	PreparedStatement ps = null;
        try {
        	System.out.println(accountCheck.getAccount());
        	String sql = "SELECT * FROM users WHERE account = ?";
        	ps = connection.prepareStatement(sql);
            ps.setString(1, accountCheck.getAccount());
			ResultSet rs = ps.executeQuery();

            List<User> ret = checkUserList(rs);
            System.out.println(ps);
            if (ret.isEmpty() == true) {
            	return null;
            }else if (2<=ret.size()) {
            	throw new IllegalStateException("2 <= userList.size()");
            }else {
            	return ret.get(0);
            }
        } catch (SQLException e){
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);

        }
    }

    public User getCheckEditUser(Connection connection, User editCheck) {  //編集重複チェック
    	PreparedStatement ps = null;
        try {
        	String sql = "SELECT * FROM users WHERE NOT (id = ?) AND account = ? ";
        	ps = connection.prepareStatement(sql);
        	ps.setInt(1, editCheck.getId());
            ps.setString(2, editCheck.getAccount());
			ResultSet rs = ps.executeQuery();
			System.out.println(ps+"編集チェック");
			List<User> ret = checkUserList(rs);
            if (ret.isEmpty() == true) {
            	return null;
            }else if (2<=ret.size()) {
            	throw new IllegalStateException("2 <= userList.size()");
            }else {
            	return ret.get(0);
            }
        } catch (SQLException e){
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);

        }
    }
    private List<User> checkUserList(ResultSet rs) throws SQLException {
    	List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");

                User user = new User();
                user.setAccount(account);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


}