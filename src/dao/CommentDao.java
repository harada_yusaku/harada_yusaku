package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {
    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");

            sql.append("text");
            sql.append(", user_id");
            sql.append(", contribution_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // text
            sql.append(", ?"); // user_id
            sql.append(", ?"); // contribution_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
//            System.out.println(ps+ "5555");

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getUserId());
            ps.setInt(3, comment.getContributionId());



            ps.executeUpdate();


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }





    public List<Comment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.contribution_id as contribution_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.user_id as user_id ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date ASC limit " + num);



            ps = connection.prepareStatement(sql.toString());  //SQL文をpsに入れた
            System.out.println(ps+ "コメント");


            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                int contributionId = rs.getInt("contribution_id");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");



                Comment comment = new Comment();
                comment.setText(text);
                comment.setContributionId(contributionId);
                comment.setName(name);
                comment.setCreated_date(createdDate);
                comment.setId(id);
                comment.setUserId(userId);


                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }






}

