package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.DeleteComment;
import exception.SQLRuntimeException;

public class DeleteCommentDao {

    public void insert(Connection connection, DeleteComment deleteComment) {

		 PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("DELETE FROM comments WHERE id = ?");

	            ps = connection.prepareStatement(sql.toString());
	            ps.setInt(1, deleteComment.getDeleteComment());

	            ps.executeUpdate();
//	            System.out.println(ps + "コメント削除");

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
// TODO 自動生成されたメソッド・スタブ

}

}
