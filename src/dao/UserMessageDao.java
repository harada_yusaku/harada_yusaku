package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {
	public List<UserMessage> getUserMessages(Connection connection, int num, UserMessage narrow) { //投稿を表示

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("contributions.id as id, ");
            sql.append("contributions.title as title, ");
            sql.append("contributions.text as text, ");
            sql.append("contributions.category as category, ");
            sql.append("users.name as name, ");
            sql.append("contributions.created_date as created_date, ");
            sql.append("contributions.user_id as user_id ");
            sql.append("FROM contributions ");
            sql.append("INNER JOIN users ");
            sql.append("ON contributions.user_id = users.id ");
            sql.append("WHERE contributions.created_date >=? AND contributions.created_date < ? ");
            if(!(narrow.getCategory() == null)) {
            	sql.append("AND contributions.category like ? ");
            }
            sql.append("ORDER BY created_date DESC limit " + num );




            ps = connection.prepareStatement(sql.toString());  //SQL文をpsに入れた
            ps.setString(1, narrow.getBegin());
            ps.setString(2, narrow.getFinish());
            if(!(narrow.getCategory() == null)) {
            	ps.setString(3, "%" + narrow.getCategory() + "%");
            	System.out.println(ps);
            }

            System.out.println(ps+"投稿");


            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");



                UserMessage message = new UserMessage();
                message.setTitle(title);
                message.setText(text);
                message.setCategory(category);
                message.setCreated_date(createdDate);
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);


                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}

