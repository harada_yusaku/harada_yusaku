package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Management;
import exception.SQLRuntimeException;

public class ManagementDao {

	public List<Management> getManagement(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT");
			sql.append(" users.id as id,");
			sql.append(" users.account as account,");
			sql.append(" users.name as name,");
			sql.append(" branches.name as branch_name,");
			sql.append(" positions.name as position_name,");
			sql.append(" users.is_stopped");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches");
			sql.append(" ON users.branch_id = branches.id");
			sql.append(" INNER JOIN positions");
			sql.append(" ON users.position_id = positions.id");


			ps = connection.prepareStatement(sql.toString());
			System.out.println(ps+ "マネジメント");

	           ResultSet rs = ps.executeQuery();
	            List<Management> ret = toManagementList(rs);

	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	 private List<Management> toManagementList(ResultSet rs)
	            throws SQLException {

	        List<Management> ret = new ArrayList<Management>();

	        try {
	            while (rs.next()) {
	            	int id = rs.getInt("id");
	                String account = rs.getString("account");
	                String name = rs.getString("name");
	                String branchName = rs.getString("branch_name");
	                String positionName = rs.getString("position_name");
	                String isStopped = rs.getString("is_stopped");




	                Management management = new Management();
	                management.setId(id);
	                management.setAccount(account);
	                management.setName(name);
	                management.setBranchName(branchName);
	                management.setPositionName(positionName);
	                management.setIsStopped(isStopped);



	                ret.add(management);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }



}
