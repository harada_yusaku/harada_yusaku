package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Delete;
import exception.SQLRuntimeException;

public class DeleteDao {

	public void insert(Connection connection, Delete delete) {

		 PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("DELETE FROM contributions WHERE id = ?");

	            ps = connection.prepareStatement(sql.toString());
	            ps.setInt(1, delete.getDelete());

	            ps.executeUpdate();

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	}







}
