
package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.User;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	request.setCharacterEncoding("UTF-8"); // このステートメントを追加

    	User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }
        UserMessage narrow = new UserMessage();

        narrow.setBegin(request.getParameter("begin"));
        narrow.setFinish(request.getParameter("finish"));
        System.out.println(request.getParameter("begin")+"ここから");
        narrow.setCategory(request.getParameter("category"));
        String begin = request.getParameter("begin");
        request.setAttribute("begin", begin);
        String finish = request.getParameter("finish");
        request.setAttribute("finish", finish);
        String category = request.getParameter("category");
        request.setAttribute("category", category);


        List<UserMessage> messages = new MessageService().getMessage(narrow);
        List<Comment> comments = new CommentService().getComment();


        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("comments", comments);



        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}