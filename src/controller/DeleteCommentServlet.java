package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DeleteComment;
import service.DeleteCommentService;

/**
 * Servlet implementation class DeleteCommentServlet
 */
@WebServlet("/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
//        	HttpSession session = request.getSession();

        	DeleteComment deleteComment = new DeleteComment();
//       	 System.out.println(request.getParameter("deleteComment") + "cd");
       	 int i = Integer.parseInt(request.getParameter("deleteComment"));
       	 deleteComment.setDeleteComment(i);

       	 new DeleteCommentService().register(deleteComment);
       	response.sendRedirect("./");
	}
}
