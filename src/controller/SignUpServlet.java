package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



        List<Branch> branches = new BranchService().getBranch();
        List<Position> positions = new PositionService().getPosition();
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);
        request.getRequestDispatcher("signup.jsp").forward(request, response);
//        System.out.println(positions.get(0).getName());

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {



	            User user = new User();
	            user.setAccount(request.getParameter("account"));
	            user.setPassword(request.getParameter("password"));
	            user.setName(request.getParameter("name"));
	            user.setBranch_id(request.getParameter("branch"));
	            user.setPosition_id(request.getParameter("position"));
	            user.setIs_stopped("0");

	            new UserService().register(user);
	            	response.sendRedirect("management");



        } else {
        	User user = new User();
        	user.setAccount(request.getParameter("account"));
            user.setName(request.getParameter("name"));
            user.setBranch_id(request.getParameter("branch"));
            user.setPosition_id(request.getParameter("position"));
            request.setAttribute("user", user);
            List<Branch> branches = new BranchService().getBranch();
            List<Position> positions = new PositionService().getPosition();
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            session.setAttribute("errorMessages", messages);
//            response.sendRedirect("signup");
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String branch = request.getParameter("branch");
        String position = request.getParameter("position");

    	User accountCheck = new User();
    	accountCheck.setAccount(request.getParameter("account"));
    	User userCheck = new UserService().checkUser(accountCheck);



        if (StringUtils.isBlank(name) == true) {
            messages.add("ユーザー名を入力してください");
        }else if (!(name.length() <= 10)) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        }
        if (userCheck != null) {
        	messages.add("そのログインIDはすでに使われています");
        }
        if (StringUtils.isBlank(account) == true) {
            messages.add("ログインIDを入力してください");
        }else if (6 > account.length()){
        	messages.add("ログインIDは6文字以上で入力してください");
        }else if (account.length() > 20 ) {
        	messages.add("ログインIDは20文字以下で入力してください");
        }else if (account.matches("\\W")) {
        	messages.add("ログインIDは半角英数字です");
        }

        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }else if (6 > password.length()) {
        	messages.add("パスワードは6文字以上で入力してください");
        }else if (password.length() > 20) {
        	messages.add("パスワードは20文字以下で入力してください");
        }else if (password.matches("[-_@+*;:#$%&A-Za-z0-9]")) {
        	messages.add("パスワードは記号を含む半角英数字です");
        }else if (!password.equals(password2)) {
        	messages.add("入力したパスワードと確認用パスワードが一致しません");
        }




        if (!branch.equals("1") && position.equals("1")) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }else if (!branch.equals("1") && position.equals("2")) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }else if (branch.equals("1") && position.equals("3")) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }else if (branch.equals("1") && position.equals("4")){
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }

        // TODO アカウントが既に利用されていないかなどの確認も必要
        if (messages.size() == 0 ) {
            return true;
        } else {
            return false;
        }
    }


}