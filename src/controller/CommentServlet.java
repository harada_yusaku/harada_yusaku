package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        	HttpSession session = request.getSession();
        	List<String> messages = new ArrayList<String>();


        	if (isValid(request, messages) == true) {
	            User user = (User) session.getAttribute("loginUser");


	            Comment comment = new Comment();


	            comment.setText(request.getParameter("text"));

	            int i = Integer.parseInt(request.getParameter("contribution_id"));
	            comment.setContributionId(i);

	            comment.setUserId(user.getId());

	            new CommentService().register(comment);
	            response.sendRedirect("./");
        	} else {
        		Comment comment = new Comment();
        		comment.setText(request.getParameter("text"));
        		session.setAttribute("comment", comment);
                session.setAttribute("errorMessages", messages);
                response.sendRedirect("./");
        	}

    }
    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String text = request.getParameter("text");
    	if (StringUtils.isBlank(text) == true) {
            messages.add("コメントを入力してください");
        }else if (text.length() > 500) {
    		messages.add("本文は500文字以下で入力してください");
    	}
    	if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }






}
