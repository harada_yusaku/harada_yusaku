package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        String st = request.getParameter("id");//idをJSPから受け取った！
        System.out.println(st);
    	if (isValid2(request, messages) == false) {
    		session.setAttribute("errorMessages", messages);
    		response.sendRedirect("management");
    		return;
		}
    	int id = Integer.parseInt(st);
		User user = new UserService().getUser(id);//idを送ってuserが返ってくる
		if(user == null) {
			response.sendRedirect("management");
			messages.add("不正なURLが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}else {
			List<Branch> branches = new BranchService().getBranch();
	        List<Position> positions = new PositionService().getPosition();
	        request.setAttribute("branches", branches);
	        request.setAttribute("positions", positions);
			request.setAttribute("user", user);//userをセット！
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}

	}
	private boolean isValid2(HttpServletRequest request, List<String> messages) {
		String st = request.getParameter("id");//idをJSPから受け取った！
		System.out.println(StringUtils.isBlank(st) == true);

		if(st == null) {
			messages.add("不正なURLが入力されました");
		}
		if(StringUtils.isBlank(st) == true) {
			messages.add("不正なURLが入力されました");
		}else if(!st.matches("[0-9]")) {
			messages.add("不正なURLが入力されました");
		}else if(st.matches("[ |　]+")){
			messages.add("不正なURLが入力されました");
		}
		if (messages.size() == 0 ) {
            return true;
        } else {
            return false;
        }
	}



    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

    	if (isValid(request, messages) == true) {

            User edit = new User();
            String st = request.getParameter("id");
    		int id = Integer.parseInt(st);
            edit.setId(id);
            edit.setAccount(request.getParameter("account"));
            edit.setPassword(request.getParameter("password"));
            edit.setName(request.getParameter("name"));
            edit.setBranch_id(request.getParameter("branch"));
            edit.setPosition_id(request.getParameter("position"));



            new UserService().userUpdate(edit);
            response.sendRedirect("management");

	  } else {
	      session.setAttribute("errorMessages", messages);
	      User user = new User();
          String st = request.getParameter("id");
  		int id = Integer.parseInt(st);
          user.setId(id);
          user.setAccount(request.getParameter("account"));
          user.setName(request.getParameter("name"));
          user.setBranch_id(request.getParameter("branch"));
          user.setPosition_id(request.getParameter("position"));
          request.setAttribute("user", user);
          List<Branch> branches = new BranchService().getBranch();
          List<Position> positions = new PositionService().getPosition();
          request.setAttribute("branches", branches);
          request.setAttribute("positions", positions);

          session.getAttribute("loginUser");
//          System.out.println("ログイン情報");
//	      response.sendRedirect("edit.jsp");
	      request.getRequestDispatcher("edit.jsp").forward(request, response);
	  }

    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String branch = request.getParameter("branch");
        String position = request.getParameter("position");

    	User editCheck = new User();
    	String st = request.getParameter("id");
		int id = Integer.parseInt(st);
//		System.out.println(id);
    	editCheck.setId(id);
    	editCheck.setAccount(request.getParameter("account"));
    	User userCheck = new UserService().checkEditUser(editCheck);



        if (StringUtils.isBlank(name) == true) {
            messages.add("ユーザー名を入力してください");
        }else if (!(name.length() <= 10)) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        }
        if (StringUtils.isBlank(account) == true) {
            messages.add("ログインIDを入力してください");
        }else if (6 > account.length()){
        	messages.add("ログインIDは6文字以上で入力してください");
        }else if (account.length() > 20 ) {
        	messages.add("ログインIDは20文字以下で入力してください");
        }else if (account.matches("\\W")) {
        	messages.add("アカウント名は半角英数字です");
        }else if (userCheck != null) {
        	messages.add("そのログインIDはすでに使われています");
        }

        if (StringUtils.isBlank(password) == true) {
        }else if (6 > password.length()) {
        	messages.add("パスワードは6文字以上で入力してください");
        }else if (password.length() > 20) {
        	messages.add("パスワードは20文字以下で入力してください");
        }else if (password.matches("[-_@+*;:#$%&A-Za-z0-9]")) {
        	messages.add("パスワードは記号を含む半角英数字です");
        }else {
        	if (!password.equals(password2)) {
            	messages.add("入力したパスワードと確認用パスワードが一致しません");
            }
        }




        if (!branch.equals("1") && position.equals("1")) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }else if (!branch.equals("1") && position.equals("2")) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }else if (branch.equals("1") && position.equals("3")) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }else if (branch.equals("1") && position.equals("4")){
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }

        // TODO アカウントが既に利用されていないかなどの確認も必要
        if (messages.size() == 0 ) {
            return true;
        } else {
            return false;
        }
    }



}
