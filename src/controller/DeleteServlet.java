package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Delete;
import service.DeleteService;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
//        	HttpSession session = request.getSession();

        	 Delete delete = new Delete();
//        	 System.out.println(request.getParameter("delete") + "ab");
        	 int i = Integer.parseInt(request.getParameter("delete"));
             delete.setDelete(i);

             new DeleteService().register(delete);


             response.sendRedirect("./");
	}
}
