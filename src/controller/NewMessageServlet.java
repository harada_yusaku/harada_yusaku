package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/contribution" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("contribution.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();
        User user = (User) session.getAttribute("loginUser");
        Message message = new Message();
        if (isValid(request, messages) == true) {


            message.setTitle(request.getParameter("title"));
            message.setCategory(request.getParameter("category"));
            message.setText(request.getParameter("message"));
            message.setUserId(user.getId());


            new MessageService().register(message);
            response.sendRedirect("./");

        } else {
            session.setAttribute("errorMessages", messages);

            message.setTitle(request.getParameter("title"));
            message.setCategory(request.getParameter("category"));
            message.setText(request.getParameter("message"));
            message.setUserId(user.getId());
            request.setAttribute("message", message);
//            response.sendRedirect("contribution");
            request.getRequestDispatcher("contribution.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String title = request.getParameter("title");
        String category = request.getParameter("category");
    	String message = request.getParameter("message");

    	if (StringUtils.isBlank(title) == true) {
            messages.add("件名を入力してください");
        }else if (title.length() > 30) {
        	messages.add("件名は30文字以下で入力してください");
        }
    	if (StringUtils.isBlank(category) == true) {
            messages.add("カテゴリを入力してください");
        }else if(category.length() > 10) {
        	messages.add("カテゴリは10文字以下で入力してください");
        }
    	if (StringUtils.isBlank(message) == true) {
            messages.add("本文を入力してください");
        }else if (message.length() > 1000) {
    		messages.add("本文は1000文字以下で入力してください");
    	}




        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


}