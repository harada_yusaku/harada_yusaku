package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Management;
import beans.User;
import service.ManagementService;
import service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<Management> managements = new ManagementService().getManagement();
        request.setAttribute("managements", managements);

        request.getRequestDispatcher("management.jsp").forward(request, response);


	}
    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

    	User is_stopped = new User();
    	System.out.println(request.getParameter("isStopped"));
    	String str = request.getParameter("isStopped");
		int i = Integer.parseInt(str);
		if(i==0) {
			is_stopped.setIs_stopped("1");
		}else if(i==1) {
			is_stopped.setIs_stopped("0");
		}else {
			System.out.println("error");
		}

		String st = request.getParameter("id");
		int id = Integer.parseInt(st);
		is_stopped.setId(id);

		new UserService().userIsStopped(is_stopped);
		response.sendRedirect("management");

    }


}
