package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Delete;
import dao.DeleteDao;

public class DeleteService {


	    public void register(Delete delete) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            DeleteDao deleteDao = new DeleteDao();
	            deleteDao.insert(connection, delete);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }


}
