
package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {
    public void register(Message message) { //新規投稿

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }





    private static final int LIMIT_NUM = 1000;


    public List<UserMessage> getMessage(UserMessage narrow) {  //投稿をTOP画面に表示
    	if(StringUtils.isEmpty(narrow.getBegin())){
    		narrow.setBegin("2019-01-01 00:00:00");
    	}else {
    		narrow.setBegin(narrow.getBegin() + " 00:00:00");
    	}
    	if(StringUtils.isEmpty(narrow.getFinish())) {
    		Calendar calendar = Calendar.getInstance();
    		String str = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    		narrow.setFinish(str +" 23:59:59");
    	}else {
    		narrow.setFinish(narrow.getFinish() + " 23:59:59");
    	}
        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, narrow);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}