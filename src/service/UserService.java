package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

    public void register(User user) {  //ユーザー新規登録

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

    public User checkUser(User accountCheck) {  //新規登録重複チェック
    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User userCheck = userDao.getCheckUser(connection, accountCheck);
    		commit(connection);
    		System.out.println(userCheck+"hhhh");

    		return userCheck;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally{
    		close(connection);
    	}
    }


    public User getUser(int id) {  //ユーザー編集画面にユーザー情報を表示する
    	  //int idを受け取りUser型で返す、getUserというメソッド
    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User user = userDao.getUser(connection, id);
    		commit(connection);

    		return user;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally{
    		close(connection);
    	}
    }

    public User checkEditUser(User editCheck) {  //編集重複チェック
    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User userCheck = userDao.getCheckEditUser(connection, editCheck);
    		commit(connection);
    		System.out.println(userCheck+"編重チェ");

    		return userCheck;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally{
    		close(connection);
    	}
    }


    public void userUpdate(User edit) {  //ユーザー編集をしてDBを上書きする

    	Connection connection =null;
    	try {
    		connection = getConnection();
    		if(!StringUtils.isBlank(edit.getPassword())) {
	    		String encPassword = CipherUtil.encrypt(edit.getPassword());
	    		edit.setPassword(encPassword);
    		}
    		UserDao userDao = new UserDao();
    		userDao.userUpdate(connection, edit);
    		commit(connection);
    	} catch (RuntimeException e){
    		rollback(connection);
    		throw e;
    	} catch(Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void userIsStopped(User isStopped) { //ユーザーのisStoppedを書き換える
    	Connection connection =null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		userDao.change(connection, isStopped);
    		commit(connection);
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }




}