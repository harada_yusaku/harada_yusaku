package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.DeleteComment;
import dao.DeleteCommentDao;

public class DeleteCommentService {
    public void register(DeleteComment deleteComment) {

        Connection connection = null;
        try {
            connection = getConnection();

            DeleteCommentDao deleteCommentDao = new DeleteCommentDao();
            deleteCommentDao.insert(connection, deleteComment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
