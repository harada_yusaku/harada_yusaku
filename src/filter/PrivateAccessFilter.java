package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = {"/management","/edit","/signup"})
public class PrivateAccessFilter implements Filter {
	public  void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws IOException,ServletException{


		HttpSession session = ((HttpServletRequest) request).getSession();
		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");


		if(!user.getPosition_id().equals("1") && !user.getPosition_id().equals("2")) {
			System.out.println(((HttpServletRequest) request).getServletPath());
			List<String> messages = new ArrayList<String>();
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) {

	}

	public void destroy() {

	}

}