package beans;

import java.io.Serializable;

public class Management implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String account;
    private String name;
    private String branchId;
    private String branchName;
    private String positionId;
    private String positionName;
    private String isStopped;
    private String edit;
    private int editOutput;

    private String password;


	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getPositionId() {
		return positionId;
	}
	public void setPositionId(String positionId) {
		this.positionId = positionId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	public int getEditOutput() {
		return editOutput;
	}
	public void setEditOutput(int editOutput) {
		this.editOutput = editOutput;
	}
	public String getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(String isStopped) {
		this.isStopped = isStopped;
	}



}
