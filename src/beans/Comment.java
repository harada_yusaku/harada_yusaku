package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

	    private static final long serialVersionUID = 1L;

	    private int id;
	    private String text;
	    private int contributionId;
	    private String name;
	    private Date created_date;
	    private int userId;


		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public int getContributionId() {
			return contributionId;
		}
		public void setContributionId(int contributionId) {
			this.contributionId = contributionId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Date getCreated_date() {
			return created_date;
		}
		public void setCreated_date(Date created_date) {
			this.created_date = created_date;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
}
