<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板</title>
    </head>
    <body>
<script type="text/javascript">


function dialog(){

	if(window.confirm('削除してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}


</script>
<div class="header">
            <a href="./">ホーム</a>
	        <a href="./logout">ログアウト</a>
	        <a href="./contribution">投稿</a>
	        <a href="./management">ユーザー管理</a>
        </div><h1><c:out value="掲示板" /></h1><br></br>
        <br>
				<c:if test="${ not empty errorMessages }">
				<div class="error">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
                </div>
                </c:if>



    </body>
</html>




    <div class="profile">
        <h2><c:out value="${loginUser.name}" /></h2>

    </div>

<div class="narrow">
<form action="./" method="get">
	<label>　この日から:<input type="date" name="begin" value="${begin}"></label><br>
	<label>　この日まで:<input type="date" name="finish" value="${finish}"></label><br>
	<label for="category">　　カテゴリ</label> <input name="category" value="${category}"/>
	<div class="search"><input type="submit" value="検索"></div>
</form>
</div>

<div class="messages">
    <c:forEach items="${messages}" var="message">
    <div class="messagebox">
            <div class="message">
                <div class="title"><c:out value="${message.title}" /></div>
                <div class="category"><c:out value="${message.category}" /></div>
                <pre>
                <div class="text"><c:out value="${message.text}" /></div>
                </pre>
                <div class="name"><c:out value="${message.name}" /></div>

                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

				<c:if test="${ message.userId == loginUser.id }">
				    <div class="deletekey">
				        <form action="delete" method="post" onSubmit="return dialog()">
		                	<input type="hidden" name= "delete" value="${message.id}">
							<input type="submit" value="投稿削除"><br>
						</form>
				        </div>
				</c:if>

				<br>
				<c:forEach items="${comments}" var="comments">
					<c:if test="${ message.id == comments.contributionId }">
						<div class="comments">
							<pre><div class="text2"><c:out value="${comments.text}" /></div></pre>
							<div class="name"><c:out value="${comments.name}" /></div>
							<div class="date"><fmt:formatDate value="${comments.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<c:if test="${ comments.userId == loginUser.id }">
				    			<div class="deleteCommentkey">
				        			<form action="deleteComment" method="post" onSubmit="return dialog()">
		                				<input type="hidden" name= "deleteComment" value="${comments.id}">
										<input type="submit" value="コメント削除">
									</form>
				        		</div>
						</c:if>
						</div>
					</c:if>
				</c:forEach>

					<div class="comment-area">
					<form action="comment" id="comment" method="post">
						<textarea name="text" cols="40" rows="2" class="tweet-box"></textarea><br>
						<input type="hidden" name= "contribution_id"value="${message.id}">
						<input type="submit" value="コメント">
					</form>
					</div>
					<br>

            </div>
            </div>
    </c:forEach>
</div>
    <footer>
    	<div class="copyright"> Copyright(c)HaradaYusaku</div>
    </footer>
