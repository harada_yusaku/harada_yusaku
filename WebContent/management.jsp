<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">


function dialog(){

	if(window.confirm('変更してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}


</script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
<body>
<div class="header">
            <a href="./">ホーム</a>
	        <a href="./logout">ログアウト</a>
	        <a href="./contribution">投稿</a>
	        <a href="./management">ユーザー管理</a>
	        <a href="signup">新規登録</a>
        </div><h1><c:out value="ユーザー管理" /></h1><br></br><br>

<div class="error">
<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            </div>
	<div class="table">
	<table border="1">
	<tr>
	 <th>ID</th>
	 <th>ユーザー名</th>
	 <th>所属</th>
	 <th>役職</th>
	 <th>ログイン状態</th>
	 <th>ユーザー編集</th>
	</tr>
	<c:forEach items="${managements}" var="managements">
		<tr>
			<td><div class="account"><c:out value="${managements.account}" /></div></td>
			<td><div class="name2"><c:out value="${managements.name}" /></div></td>
			<td><div class="branch"><c:out value="${managements.branchName}" /></div></td>
			<td><div class="position"><c:out value="${managements.positionName}" /></div></td>
				<td>
				<c:if test="${managements.id != loginUser.id }">
					<form action="management" method="post" onSubmit="return dialog()">
						<c:if test="${managements.isStopped == 0}">
						<input type="submit" value="ログイン可">
						</c:if>
						<c:if test="${managements.isStopped == 1}">
						<input type="submit" value="ログイン不可">
						</c:if>
						<input type="hidden" name="id" value="${managements.id}">
						<input type="hidden" name="isStopped" value="${managements.isStopped}">
					</form>
				</c:if>

				<c:if test="${managements.id == loginUser.id }">
					<c:out value="ログイン中" />
				</c:if>
				</td>


				<td><a href="./edit?id=${managements.id}">編集</a></td>
		</tr>
	</c:forEach>
	</table>
	</div>
<div class="reverse"><a href="./">戻る</a></div><br />
						<div class="copyright">Copyright(c)HaradaYusaku</div>
</body>
</html>