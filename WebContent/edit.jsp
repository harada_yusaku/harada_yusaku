<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
<body>
 <div class="header">
            <a href="./">ホーム</a>
	        <a href="./logout">ログアウト</a>
	        <a href="./contribution">投稿</a>
	        <a href="./management">ユーザー管理</a>
		 </div>
        <h1><c:out value="ユーザー編集" /></h1><br></br>
<div class="error">
<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
                </c:if>
                </div>

	<div class="edit">
		<form action="edit" method="post">
			<label for="name">　　　ユーザー名</label><input name="name" id="name" value="${user.name}"/><br />
			<label for="acccount">　　　ログインID</label><input name="account" id="account" value="${user.account}"/><br />
			<label for="password">　　　パスワード</label><input name="password" type="password" id="password"/><br />
			<label for="password2">確認用パスワード</label> <input name="password2" type="password"  /><br />
			<c:if test="${user.id != loginUser.id }">
			<SELECT name="branch">
                	<c:forEach items="${branches}" var="branch">
                	<c:choose>
                		<c:when test="${branch.id == user.branch_id}">
                			<option value="${branch.id}" selected>${branch.name}</option>
                		</c:when>
                	<c:otherwise>
						<option value="${branch.id}">${branch.name}</option>
					</c:otherwise>
					</c:choose>
					</c:forEach>
				</SELECT><br />
                <SELECT name="position">
                <c:forEach items="${positions}" var="positions">
                	<c:choose>
                		<c:when test="${positions.id == user.position_id}">
                			<option value="${positions.id}" selected>${positions.name}</option>
                		</c:when>
                	<c:otherwise>
						<option value="${positions.id}">${positions.name}</option>
					</c:otherwise>
					</c:choose>
				</c:forEach>
			</SELECT><br />
			</c:if>
			<c:if test="${user.id == loginUser.id }">
				<div class="branch"><c:out value="${loginUser.branchName}" /></div>
			<div class="position"><c:out value="${loginUser.positionName}" /></div>
			<input type="hidden" name="branch" value="${user.branch_id}">
			<input type="hidden" name="position" value="${user.position_id}">
			</c:if>
			<input type="hidden" name="id" value="${user.id}"><br />
			<input type="submit" value="変更"/>
		</form>
		</div>
<div class="reverse"><a href="./management">戻る</a></div><br />
						<div class="copyright">Copyright(c)HaradaYusaku</div>
</body>

</html>