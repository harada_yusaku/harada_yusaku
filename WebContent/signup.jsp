<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規登録</title>
    </head>
    <body>
     <div class="header">
            <a href="./">ホーム</a>
	        <a href="./logout">ログアウト</a>
	        <a href="./contribution">投稿</a>
	        <a href="./management">ユーザー管理</a>

        </div><h1><c:out value="新規登録" /></h1><br></br><br>

        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
            <div class="error">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
                </div>
            </c:if>


        <div class="signup">
            <form action="signup" method="post">
                <br /> <label for="name">　　　ユーザー名</label> <input name="name" id="name" value="${user.name}"/>
                <br /> <label for="account">　　　ログインID</label> <input name="account"id="account" value="${user.account}"/>
                <br /> <label for="password">　　　パスワード</label> <input name="password" type="password"  /><br />
                <label for="password2">確認用パスワード</label> <input name="password2" type="password"  /><br />
                <SELECT name="branch">
                	<c:forEach items="${branches}" var="branch">
					<c:choose>
                		<c:when test="${user.branch_id == branch.id}">
                			<option value="${branch.id}" selected>${branch.name}</option>
                		</c:when>
                	<c:otherwise>
						<option value="${branch.id}">${branch.name}</option>
					</c:otherwise>
					</c:choose>
					</c:forEach>
				</SELECT><br />
                <SELECT name="position">
                <c:forEach items="${positions}" var="positions">
				<c:choose>
                		<c:when test="${positions.id == user.position_id}">
                			<option value="${positions.id}" selected>${positions.name}</option>
                		</c:when>
                	<c:otherwise>
						<option value="${positions.id}">${positions.name}</option>
					</c:otherwise>
					</c:choose>
				</c:forEach>
			</SELECT><br />
                <br /> <input type="submit" value="登録" /> <br />
            </form>
            </div>
            <div class="reverse"><a href="./management">戻る</a></div><br />
						<div class="copyright">Copyright(c)HaradaYusaku</div>
        </div>
    </body>
</html>