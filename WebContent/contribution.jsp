<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>投稿フォーム</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
<body>
<div class="header">
            <a href="./">ホーム</a>
	        <a href="./logout">ログアウト</a>
	        <a href="./management">ユーザー管理</a>
</div><h1><c:out value="投稿フォーム" /></h1><br></br><br></br>

<br></br>
			<div class="form-area">
			<div class="error">
				<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            </div>
					<form action="contribution" method="post">
					<div class="contribution">
						<br /><label for="tilte">件名（30文字以下）</label><br /><input name="title" id="title" value="${message.title}"/><br />
						<br /><label for="category">カテゴリ（10文字以下）</label><br /><input name="category" id="category" value="${message.category}"/><br />
						<br /><br />
						<label for="message">本文（1000文字以下）</label><br /><textarea name="message" cols="100" rows="5" class="tweet-box">${message.text}</textarea>
						<br />

						<input type="submit" value="投稿">
						</div>

						 <br /><div class="reverse"><a href="./">戻る</a></div><br />
						<div class="copyright">Copyright(c)HaradaYusaku</div>
					</form>

			</div>


</body>
</html>
